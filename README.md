# Flask
Projetos de estudo do flask python 3.6.8
### Dependencies
`λ pip install flask`

### Install Python shell
`pip install ipython flask-shell-ipython`
### Commands
`python [arquivo].py`
## Comands Python Shell
* use python shell to test the code as if it were in the browser
* `export FLASK_APP=[file appyFlask]`
* `flask shell` → Open flask shell
* `app.url_map` → Mapping all routes
* `client = app.test_client()` → To use HTTP methods
* `client.[method]([route], follow_redirects=True)` Ex: client.get('user/david', follow_redirects=True)
