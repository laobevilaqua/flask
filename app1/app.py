import db
from flask import Flask, abort, url_for

# `app = Flask('app1')` not good practice
app = Flask(__name__)

# VIEWS
@app.route('/')
def index():
    html = ['<ul>']
    for username, user in db.users.items():
        html.append(
            f"<li><a href='{url_for('user',username=username)}'>{user['name']}</a></li>"
        )
    html.append('</ul>')
    return '\n'.join(html)

def profile(username):
    user = db.users.get(username)
    if user:
        return f"""
            <h1>{user['name']}</h1>
            <img src="{user['image']}"><br>
            Telefone: {user['tel']}<br>
            <a href="/">Voltar</a>
        """
    else:
        return abort(404, "User not found")

@app.route('/hi')
def hi():
    return "Hi"

# Utiliza-se o endponter para que o elemento q usa a rota não necessite de alteração caso uma rota mude de nome
# <[type]: [variable]> verifica se a variavel pode ser convertido para o tipo antes de passar para rota. OBS: para não chamar o recurso
app.add_url_rule('/user/<string: username>/', view_func=profile, endpoint='user')

# compiler when saving
app.run(use_reloader=True)
